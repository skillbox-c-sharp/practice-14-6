﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class LegalClient : Client
    {
        public LegalClient(string name) 
            : base(name)
        {
            InterestRate = 0.2f ;
        }

        public LegalClient(string name, int id, Account account, DepositAccount depositAccount, float interestRate)
            : base(name, id, account, depositAccount, interestRate) { }
    }
}
