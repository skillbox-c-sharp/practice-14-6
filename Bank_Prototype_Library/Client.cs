﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class Client : ITopUpAccount<Client>, ITransfer<Client>
    {
        private static int currentId;

        public int Id { get; private set; }
        public string Name { get; private set; }
        public Account AccountClient { get; private set; }
        public float InterestRate { get; protected set; }
        public DepositAccount DepositAccountClient { get; private set; }

        public Client(string name)
        {
            Name = name;
            Id = currentId++;
            AccountClient = new Account(Id);
            DepositAccountClient = new DepositAccount(Id);
            InterestRate = 0.1f;
        }

        public Client(string name, int id, Account account, DepositAccount depositAccount, float interestRate)
        {
            Name = name;
            Id = currentId++;
            AccountClient = account;
            DepositAccountClient = depositAccount;
            InterestRate = interestRate;
        }

        static Client()
        {
            currentId = 0;
        }

        public void CloseAccount(bool isDeposit)
        {
            if(isDeposit) AccountAction<DepositAccount>.CloseAccount(DepositAccountClient);
            else AccountAction<Account>.CloseAccount(AccountClient);
        }

        public virtual void OpenAccount(bool isDeposit)
        {
            if (isDeposit) AccountAction<DepositAccount>.OpenAccount(DepositAccountClient);
            else AccountAction<Account>.OpenAccount(AccountClient);
        }

        public virtual void TopUpBalance(double value, bool isDeposit)
        {
            if (isDeposit) AccountAction<DepositAccount>.TopUpAccount(DepositAccountClient, value);
            else AccountAction<Account>.TopUpAccount(AccountClient, value);
        }

        public void WithdrawBalance(double value, bool isDeposit)
        {
            if (isDeposit) AccountAction<DepositAccount>.WithdrawAccount(DepositAccountClient, value);
            else AccountAction<Account>.WithdrawAccount(AccountClient, value);
        }

        public virtual double CheckAccumulation(bool isDeposit)
        {
            if (isDeposit) return AccountAction<DepositAccount>.CheckAccumulation(DepositAccountClient, InterestRate);
            else return AccountAction<Account>.CheckAccumulation(AccountClient, InterestRate);
        }

        public void TransferBalance(Client clientTo, double value, bool isDeposit)
        {
            if (isDeposit) AccountAction<DepositAccount>.TransferAccount(DepositAccountClient, clientTo.DepositAccountClient, value);
            else AccountAction<Account>.TransferAccount(AccountClient, clientTo.AccountClient, value);
        }
    }
}
