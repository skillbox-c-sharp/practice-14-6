﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Challenge1
{
    public static class FileWork
    {
        public static List<Client> SetAllClientsList()
        {
            CheckFilesExist();

            List<Client> clients = new List<Client>();
            List<Account> accounts = SetAccounts();
            List<DepositAccount> depositAccounts = SetDepositAccounts();

            XDocument xdoc = XDocument.Load("clients.xml");
            foreach (XElement element in xdoc.Element("Clients").Elements("Client"))
            {
                XAttribute nameAttribute = element.Attribute("Name");
                XElement id = element.Element("Id");
                XElement interestRate = element.Element("InterestRate");
                XElement clientType = element.Element("ClientType");

                int currentId = int.Parse(id.Value);

                var query = accounts.Where(p => p.ClientId == currentId);
                Account currentAcc = query.FirstOrDefault();

                var queryDeposit = depositAccounts.Where(p => p.ClientId == currentId);
                DepositAccount currentDepositAcc = queryDeposit.FirstOrDefault();

                switch (clientType.Value.ToString())
                {
                    case "Client":
                        clients.Add(new Client(nameAttribute.Value, currentId, currentAcc, currentDepositAcc, float.Parse(interestRate.Value, CultureInfo.InvariantCulture.NumberFormat)));
                        break;
                    case "VIPClient":
                        clients.Add(new VIPClient(nameAttribute.Value, currentId, currentAcc, currentDepositAcc, float.Parse(interestRate.Value, CultureInfo.InvariantCulture.NumberFormat)));
                        break;
                    case "LegalClient":
                        clients.Add(new LegalClient(nameAttribute.Value, currentId, currentAcc, currentDepositAcc, float.Parse(interestRate.Value, CultureInfo.InvariantCulture.NumberFormat)));
                        break;
                    default:
                        break;
                }
            }
            return clients;
        }

        private static void CheckFilesExist()
        {
            if (!File.Exists("clients.xml"))
            {
                File.Create("clients.xml").Close();
                File.Create("accounts.xml").Close();
                File.Create("depositAccounts.xml").Close();

                List<Client> randomClients = new List<Client>();

                for (int i = 0; i < 5; i++)
                {
                    randomClients.Add(RandomClient.CreateRandomClient());
                }

                SaveToFiles(randomClients);
            }
        }


        public static void SaveToFiles(List<Client> clients)
        {
            List<Account> accounts = GetAccountsList(clients);
            List<DepositAccount> depositAccounts = GetDepositAccountsList(clients);

            SaveToClientsFile(clients);
            SaveToAccountsFile(accounts);
            SaveToDepositAccountsFile(depositAccounts);
        }

        private static void SaveToClientsFile(List<Client> clients)
        {
            XElement xClients = new XElement("Clients");
            for (int i = 0; i < clients.Count; i++)
            {
                XElement clientElement = new XElement("Client",
                        new XAttribute($"Name", clients[i].Name),
                        new XElement("Id", clients[i].Id),
                        new XElement("InterestRate", clients[i].InterestRate),
                        new XElement("ClientType", clients[i].GetType().Name));
                xClients.Add(clientElement);
            }
            xClients.Save("clients.xml");
        }

        private static void SaveToAccountsFile(List<Account> accounts)
        {
            XElement xAccounts = new XElement("Accounts");
            for (int i = 0; i < accounts.Count; i++)
            {
                string changes = string.Join("##", accounts[i].ChangeLog);

                XElement accountElement = new XElement("Account",
                            new XElement($"AccountNumber", accounts[i].AccountNumber),
                            new XElement("ClientId", accounts[i].ClientId),
                            new XElement("Balance", accounts[i].Balance),
                            new XElement("IsOpen", accounts[i].IsOpen),
                            new XElement("ChangeLog", changes));
                xAccounts.Add(accountElement);
            }
            xAccounts.Save("accounts.xml");
        }

        private static void SaveToDepositAccountsFile(List<DepositAccount> depositAccounts)
        {
            XElement xAccounts = new XElement("Accounts");
            for (int i = 0; i < depositAccounts.Count; i++)
            {
                string changes = string.Join("##", depositAccounts[i].ChangeLog);

                XElement accountElement = new XElement("Account",
                            new XElement($"AccountNumber", depositAccounts[i].AccountNumber),
                            new XElement("ClientId", depositAccounts[i].ClientId),
                            new XElement("Balance", depositAccounts[i].Balance),
                            new XElement("IsOpen", depositAccounts[i].IsOpen),
                            new XElement("ChangeLog", changes));
                        
                xAccounts.Add(accountElement);
            }
            xAccounts.Save("depositAccounts.xml");
        }

        private static List<Account> GetAccountsList(List<Client> clients)
        {
            List<Account> accounts = new List<Account>();

            foreach (Client client in clients)
            {
                accounts.Add(client.AccountClient);
            }

            return accounts;
        }

        private static List<DepositAccount> GetDepositAccountsList(List<Client> clients)
        {
            List<DepositAccount> accounts = new List<DepositAccount>();

            foreach (Client client in clients)
            {
                accounts.Add(client.DepositAccountClient);
            }

            return accounts;
        }

        private static List<Account> SetAccounts()
        {
            List<Account> accounts = new List<Account>();

            XDocument xdoc = XDocument.Load("accounts.xml");

            foreach (XElement element in xdoc.Element("Accounts").Elements("Account"))
            {
                XElement accountNumber = element.Element("AccountNumber");
                XElement clientId = element.Element("ClientId");
                XElement balance = element.Element("Balance");
                XElement isOpen = element.Element("IsOpen");
                XElement changeLog = element.Element("ChangeLog");

                List<string> changes = new List<string>();
                changes.AddRange(changeLog.Value.Split("##"));
                changes.RemoveAll(x => string.IsNullOrEmpty(x));

                accounts.Add(new Account(int.Parse(clientId.Value), int.Parse(accountNumber.Value), 
                    double.Parse(balance.Value), bool.Parse(isOpen.Value), changes));
            }

            return accounts;
        }
        private static List<DepositAccount> SetDepositAccounts()
        {
            List<DepositAccount> depositAccounts = new List<DepositAccount>();

            XDocument xdoc = XDocument.Load("depositAccounts.xml");

            foreach (XElement element in xdoc.Element("Accounts").Elements("Account"))
            {
                XElement accountNumber = element.Element("AccountNumber");
                XElement clientId = element.Element("ClientId");
                XElement balance = element.Element("Balance");
                XElement isOpen = element.Element("IsOpen");
                XElement changeLog = element.Element("ChangeLog");

                List<string> changes = new List<string>();
                changes.AddRange(changeLog.Value.Split("##"));
                changes.RemoveAll(x => string.IsNullOrEmpty(x));


                depositAccounts.Add(new DepositAccount(int.Parse(clientId.Value), int.Parse(accountNumber.Value), 
                    double.Parse(balance.Value), bool.Parse(isOpen.Value), changes));
            }

            return depositAccounts;

        }
    }
}
