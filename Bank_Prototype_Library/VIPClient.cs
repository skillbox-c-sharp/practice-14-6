﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class VIPClient : Client
    {
        public VIPClient(string name)
            : base(name) 
        {
            InterestRate = 0.15f;
            TopUpBalance(1000, false);
        }

        public VIPClient(string name, int id, Account account, DepositAccount depositAccount, float interestRate)
            : base(name, id, account, depositAccount, interestRate) { }

        public override void OpenAccount(bool isDeposit)
        {
            if (isDeposit)
            {
                AccountAction<DepositAccount>.OpenAccount(DepositAccountClient);
                TopUpBalance(1000, true);
            }
            else
            {
                AccountAction<Account>.OpenAccount(AccountClient);
                TopUpBalance(1000, false);
            }
        }


        public override void TopUpBalance(double value, bool isDeposit)
        {
            value += value * 0.1;
            if (isDeposit) AccountAction<DepositAccount>.TopUpAccount(DepositAccountClient, value);
            else AccountAction<Account>.TopUpAccount(AccountClient, value);
        }
    }
}
