﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public interface ITransfer<in T>
    {
        void TransferBalance(T clientTo, double value, bool isDeposit);
    }
}
