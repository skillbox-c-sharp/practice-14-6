﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class AccountNotOpenException : Exception
    {
        public AccountNotOpenException(string message)
         : base(message) { }
    }
}
