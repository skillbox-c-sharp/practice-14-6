﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public class Account
    {
        public event Action OnAccountChanged;
        public event Action<string> OnTransfered;
        public event Action<string> OnGetTransfered;

        private static int currentAccountNumber;

        public int AccountNumber { get; private set; }
        public int ClientId { get; private set; }
        public double Balance { get; private set; }
        public bool IsOpen { get; protected set; }
        public List<string> ChangeLog { get; protected set; }

        public Account(int clientId)
        {
            ClientId = clientId;
            AccountNumber = currentAccountNumber++;
            Balance = 0;
            IsOpen = true;
            ChangeLog = new List<string>() { "Cчет создан" };
        }
        public Account(int clientId, int accountNumber, double balance, bool isOpen, List<string> changeLog)
        {
            ClientId = clientId;
            AccountNumber = accountNumber;
            Balance = balance;
            IsOpen = isOpen;
            ChangeLog = changeLog;
        }

        static Account()
        {
            currentAccountNumber = 100000;
        }

        public void CloseAccount()
        {
            if (IsOpen)
            {
                IsOpen = false;
                Balance = 0;
                ChangeLog.Add("Счет закрыт");
                OnAccountChanged?.Invoke();
            }
        }

        public void OpenAccount()
        {
            if (!IsOpen)
            {
                IsOpen = true;
                ChangeLog.Add("Счет открыт");
                OnAccountChanged?.Invoke();
            }
            
        }

        public void TopUpAccount(double value)
        {
            if (IsOpen)
            {
                Balance += value;
                ChangeLog.Add($"Счет пополнен на {value}");
                OnAccountChanged?.Invoke();
            }
        }

        public void WithdrawAccount(double value)
        {
            if (IsOpen && Balance >= value)
            {
                Balance -= value;
                ChangeLog.Add($"Со счета снято {value}");
                OnAccountChanged?.Invoke();
            }
        }

        public virtual double CheckAccumulation(float interestRate)
        {
            if (IsOpen)
            {
                return Balance + Balance * interestRate;
            }
            else
            {
                return 0;
            }
        }

        public void TransferAccount(Account accTo, double value)
        {
            if (IsOpen && Balance >= value && accTo.IsOpen)
            {
                accTo.Balance += value;
                Balance -= value;
                ChangeLog.Add($"Перевод {value} клиенту {accTo.AccountNumber}");
                OnAccountChanged?.Invoke();
                OnTransfered?.Invoke($"Перевод {value} клиенту {accTo.AccountNumber}");
                OnGetTransfered += accTo.OnGetTransfer;
                OnGetTransfered?.Invoke($"Перевод {value} получен от {AccountNumber}");
                OnGetTransfered -= accTo.OnGetTransfer;
            }
        }

        public void OnGetTransfer(string msg)
        {
            ChangeLog.Add(msg);
        }
    }
}
