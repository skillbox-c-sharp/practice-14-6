﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public static class ClientExtension
    {
        public static void TransferBalance(this double value, Client clientFrom, Client clientTo, bool isDeposit)
        {
            if (isDeposit) AccountAction<DepositAccount>.TransferAccount(clientFrom.DepositAccountClient, clientTo.DepositAccountClient, value);
            else AccountAction<Account>.TransferAccount(clientFrom.AccountClient, clientTo.AccountClient, value);
        }
    }
}
