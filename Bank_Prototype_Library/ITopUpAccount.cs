﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public interface ITopUpAccount<out T>
    {
        void TopUpBalance(double value, bool isDeposit);
    }
}
