﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public static class AccountAction<T>
        where T : Account
    {
        static public void CloseAccount(T acc)
        {
            acc.CloseAccount();
        }

        static public void OpenAccount(T acc)
        {
            acc.OpenAccount();
        }

        static public void TopUpAccount(T acc, double value)
        {
            acc.TopUpAccount(value);
        }

        static public void WithdrawAccount(T acc, double value)
        {
            acc.WithdrawAccount(value);
        }

        static public double CheckAccumulation(T acc, float interestRate)
        {
            return acc.CheckAccumulation(interestRate);
        }

        static public void TransferAccount(T acc, T accTo, double value)
        {
            acc.TransferAccount(accTo, value);
        }
    }
}
