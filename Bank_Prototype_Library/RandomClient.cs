﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge1
{
    public static class RandomClient
    {
        public static Client CreateRandomClient()
        {
            Random random = new Random();
            Client randomClient;
            switch (random.Next(0, 3))
            {
                case 0:
                    randomClient = new Client(GetRandomName());
                    break;
                case 1:
                    randomClient = new VIPClient(GetRandomName());
                    break;
                case 2:
                    randomClient = new LegalClient(GetRandomName());
                    break;
                default:
                    randomClient = new Client(GetRandomName());
                    break;
            }
            return randomClient;
        }

        private static string GetRandomName()
        {
            Random random = new Random();
            string[] randomNames = { "Иван", "Сергей", "Максим", "Семен", "Валерий", "Игорь", "Вячеслав", "Артем" };
            return randomNames[random.Next(randomNames.Length)];
        }
    }
}
